<?php
namespace Fmontalbano\Typo3PrettyExtensionLive\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author Florian 
 */
class ThingsDisplayerControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Fmontalbano\Typo3PrettyExtensionLive\Controller\ThingsDisplayerController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Fmontalbano\Typo3PrettyExtensionLive\Controller\ThingsDisplayerController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

}
