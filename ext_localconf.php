<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Fmontalbano.Typo3PrettyExtensionLive',
            'Livethingsdisplayer',
            [
                'ThingsDisplayer' => 'displayTitle, haveFun'
            ],
            // non-cacheable actions
            [
                'ThingsDisplayer' => ''
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    livethingsdisplayer {
                        iconIdentifier = typo_3_pretty_extension_live-plugin-livethingsdisplayer
                        title = LLL:EXT:typo_3_pretty_extension_live/Resources/Private/Language/locallang_db.xlf:tx_typo_3_pretty_extension_live_livethingsdisplayer.name
                        description = LLL:EXT:typo_3_pretty_extension_live/Resources/Private/Language/locallang_db.xlf:tx_typo_3_pretty_extension_live_livethingsdisplayer.description
                        tt_content_defValues {
                            CType = list
                            list_type = typo3prettyextensionlive_livethingsdisplayer
                        }
                    }
                }
                show = *
            }
       }'
    );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
		
			$iconRegistry->registerIcon(
				'typo_3_pretty_extension_live-plugin-livethingsdisplayer',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:typo_3_pretty_extension_live/Resources/Public/Icons/user_plugin_livethingsdisplayer.svg']
			);
		
    }
);
