<?php
namespace Fmontalbano\Typo3PrettyExtensionLive\Domain\Model;

/***
 *
 * This file is part of the "typo-3-pretty-extension" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Florian
 *
 ***/

/**
 * ThingsDisplayer
 */
class ThingsDisplayer extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    }
