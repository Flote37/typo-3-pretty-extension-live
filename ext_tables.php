<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Fmontalbano.Typo3PrettyExtensionLive',
            'Livethingsdisplayer',
            'LIVE Things Displayer'
        );

        $pluginSignature = str_replace('_', '', 'typo_3_pretty_extension_live') . '_livethingsdisplayer';
        $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:typo_3_pretty_extension_live/Configuration/FlexForms/flexform_livethingsdisplayer.xml');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('typo_3_pretty_extension_live', 'Configuration/TypoScript', 'typo-3-pretty-extension-live');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_typo3prettyextensionlive_domain_model_thingsdisplayer', 'EXT:typo_3_pretty_extension_live/Resources/Private/Language/locallang_csh_tx_typo3prettyextensionlive_domain_model_thingsdisplayer.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_typo3prettyextensionlive_domain_model_thingsdisplayer');

    }
);
