
plugin.tx_typo3prettyextensionlive_livethingsdisplayer {
    view {
        # cat=plugin.tx_typo3prettyextensionlive_livethingsdisplayer/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:typo_3_pretty_extension_live/Resources/Private/Templates/
        # cat=plugin.tx_typo3prettyextensionlive_livethingsdisplayer/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:typo_3_pretty_extension_live/Resources/Private/Partials/
        # cat=plugin.tx_typo3prettyextensionlive_livethingsdisplayer/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:typo_3_pretty_extension_live/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_typo3prettyextensionlive_livethingsdisplayer//a; type=string; label=Default storage PID
        storagePid =
    }
}
